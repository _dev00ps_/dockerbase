def sticks_game(number_of_sticks, number_of_players):
    # sticks_in_process = number_of_sticks
    player = 1
    while number_of_sticks > 0:
        # for n in range(1, number_of_players+1):
        print(f'{number_of_sticks} sticks remaining.')
        taken = int(input('How many sticks you want?\n'))
        if not can_take(taken):
            print('Allowed to take 1,2,3.')
            continue

        print(f'{player} player get {taken} sticks')
        number_of_sticks -= taken

        if end_of_game(number_of_sticks):
            print(f'Player {player} loose!')
            quit()

        player = switch_player(number_of_players, player)
        print("🐍 File: basics/sticksgame.py | Line: 21 | sticks_game ~ player", player)


def can_take(sticks):
    return sticks >= 1 and sticks <= 3


def switch_player(number_of_players, turn):
    return 1 if turn == number_of_players else turn+1


def end_of_game(sticks):
    return sticks <= 0


def get_params():
    while True:
        try:
            number_of_players = int(input('How many players here?\n'))
            number_of_sticks = int(input('How many sticks in play?\n'))
            return (number_of_sticks, number_of_players)
        except:
            print('Not a number! Please try again.')
            continue


def game_start():
    (number_of_sticks, number_of_players) = get_params()
    print('Lets start!')
    sticks_game(number_of_sticks, number_of_players)


game_start()
